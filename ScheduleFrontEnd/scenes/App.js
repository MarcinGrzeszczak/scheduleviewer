import React from 'react'
import ReactDOM  from 'react-dom'
import Calendar  from '../components/calendar/Calendar'
import io from 'socket.io-client'

class App extends React.Component {

    render() {
        return (
            <Calendar/>
        )
    }
}

ReactDOM.render(App, document.getElementById('root'))


