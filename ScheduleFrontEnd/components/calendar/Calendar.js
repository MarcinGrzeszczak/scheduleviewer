import React  from 'react'
import FullCalendar from '@fullcalendar/react'
import config from './calendarConfig'

class Calendar extends React.Component {

    constructor(props) {
      super(props)
      this.state = {
          allEvents: this.props.events,
          viewName : window.innerWidth > window.innerHeight ? config.viewNames.horizontal : config.viewNames.vertical
      }
    }

    updateDimensions(view) {
        if(window.innerWidth > window.innerHeight)
            this.setState({viewName: config.viewNames.horizontal})
        else
            this.setState({viewName: config.viewNames.vertical})
      }

    
    componentDidMount() {
      socket.on('eventsUpdate', data => {
        events = data
    })
    
      }
    /*
      componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
      }
    */

    render() {
        return (
        <div>
            <FullCalendar
            locale= {config.locale}
            initialView = {this.state.viewName}
            height = {config.height}
            windowResize = {arg => this.updateDimensions(arg)}
            views= {config.views}
            plugins = {config.plugins}
            duration = {config.duration}
            themeSystem = {config.themeSystem}
            allDaySlot = {config.allDaySlot}
            hiddenDays = {config.hiddenDays}
            weekends={config.weekends}
            slotDuration= {config.slotDuration}
            slotMaxTime= {config.maxTime}
            slotMinTime = {config.minTime}
            events = {this.state.allEvents} />
        </div>
        )
    }
}

export default Calendar