import timeGridPlugin from '@fullcalendar/timegrid'
import bootstrapPlugin from '@fullcalendar/bootstrap'

import plLocale from '@fullcalendar/core/locales/pl'

export default {
    locale: plLocale,
    viewNames : {
        horizontal: 'timeGridWeek',
        vertical: 'verticalView'
    },
    views: {
        verticalView: {
            type: 'timeGrid',
            duration: {days: 1}
        }
    },
    height: '100%',
    plugins: [timeGridPlugin, bootstrapPlugin],
    allDaySlot: false,
    themeSystem: 'bootstrap',
    hiddenDays: [5],
    weekends: false,
    slotDuration: '00:20:00',
    minTime:'6:00:00',
    maxTime:'20:30:00'   
}