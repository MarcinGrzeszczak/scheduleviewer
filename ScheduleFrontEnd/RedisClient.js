const redis = require('redis')

const TYPES = {
    EVENTS: 'events'
}
class RedisClient {
    constructor() {
        this.client = redis.createClient({
            host:'redis-server'
        })
    }

    subscribe(channel) {
        this.client.subscribe(channel)
    }

    messages(callback) {
        this.client.on('message', (channel,value) => {
            if(channel === TYPES.EVENTS)
                callback(value)
        })
    }
}

module.exports = {
    Client: RedisClient,
    TYPES
}