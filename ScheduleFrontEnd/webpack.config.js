const path = require('path')

module.exports = {
    entry: path.join(process.cwd(),'scenes','App.js'),
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader'
                }]
            },
            {
                test:/\.css$/i,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                ]
            }
        ]
    },

    output: {
        path: path.join(process.cwd(),'public'),
        publicPath: path.join(process.cwd(),'public'),
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: './public'
    }
}
