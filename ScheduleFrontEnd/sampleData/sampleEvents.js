export default [ { title: '23/24 Informacja dydaktyczna Dziekan',
color: '#43a047',
start: '2019-10-09T08:25:00.000Z',
end: '2019-10-09T09:10:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23/24 Probabilistyka i statystyka w',
color: '#43a047',
start: '2019-10-09T09:20:00.000Z',
end: '2019-10-09T10:50:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23/24 Podstwy systemów operacyjnych w',
color: '#43a047',
start: '2019-10-09T11:10:00.000Z',
end: '2019-10-09T12:40:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23/24 Podstwy telekomunikacji w',
color: '#43a047',
start: '2019-10-09T12:50:00.000Z',
end: '2019-10-09T14:20:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23/24 Technologie internetowe w',
color: '#43a047',
start: '2019-10-09T14:30:00.000Z',
end: '2019-10-09T16:00:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '13 Języki i metody programowania JAVA lab',
color:'#1e88e5',
start: '2019-10-10T06:00:00.000Z',
end: '2019-10-10T07:30:00.000Z',
attendees: [ { name: '3S inf B' } ] },
{ title: '23/24 Sieci komputerowe w',
color: '#43a047',
start: '2019-10-10T07:40:00.000Z',
end: '2019-10-10T09:10:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23/24 Języki i metody programowania JAVA w',
color: '#43a047',
start: '2019-10-10T09:20:00.000Z',
end: '2019-10-10T10:50:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '13 Języki i metody programowania JAVA lab',
color:'#1e88e5',
start: '2019-10-10T11:10:00.000Z',
end: '2019-10-10T12:40:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title: '23 Język angielski ćw',
start: '2019-10-10T11:10:00.000Z',
end: '2019-10-10T12:40:00.000Z',
attendees: [ { name: '3S inf B' } ] },
{ title:
 'hala "B" /Młodych Techników ZS 18/ zajęcia doskonalące dla osób niepełnosprawnych i UKS',
start: '2019-10-10T16:00:00.000Z',
end: '2019-10-10T17:30:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23/24 Technologie internetowe w',
color: '#43a047',
start: '2019-10-14T06:00:00.000Z',
end: '2019-10-14T07:30:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23/24 Języki i metody programowania JAVA w',
color: '#43a047',
start: '2019-10-14T07:40:00.000Z',
end: '2019-10-14T09:10:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23/24 Sieci komputerowe w',
color: '#43a047',
start: '2019-10-14T09:20:00.000Z',
end: '2019-10-14T10:50:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23 Probabilistyka i statystyka cw',
color:'#8e24aa',
start: '2019-10-15T06:00:00.000Z',
end: '2019-10-15T07:30:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title: '13 Podstwy telekomunikacji cw',
color:'#8e24aa',
start: '2019-10-15T06:00:00.000Z',
end: '2019-10-15T07:30:00.000Z',
attendees: [ { name: '3S inf B' } ] },
{ title: '13 Języki i metody programowania JAVA lab',
color:'#1e88e5',
start: '2019-10-15T07:40:00.000Z',
end: '2019-10-15T09:10:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title: '23/24 Języki i metody programowania JAVA w',
start: '2019-10-15T09:20:00.000Z',
end: '2019-10-15T10:50:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23 Język angielski ćw',
color:'#8e24aa',
start: '2019-10-15T11:10:00.000Z',
end: '2019-10-15T12:40:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title: '13 Języki i metody programowania JAVA lab',
color:'#1e88e5',
start: '2019-10-15T11:10:00.000Z',
end: '2019-10-15T12:40:00.000Z',
attendees: [ { name: '3S inf B' } ] },
{ title: '23/24 Podstwy systemów operacyjnych w',
color: '#43a047',
start: '2019-10-16T06:00:00.000Z',
end: '2019-10-16T07:30:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: ' 12 Technologie internetowe lab',
start: '2019-10-16T07:40:00.000Z',
end: '2019-10-16T09:10:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title: '13 Podstawy telekomunikacji ćw',
color:'#8e24aa',
start: '2019-10-16T07:40:00.000Z',
end: '2019-10-16T09:10:00.000Z',
attendees: [ { name: '3S inf B' } ] },
{ title: ' 16 Podstwy systemów operacyjnych lab',
color:'#1e88e5',
start: '2019-10-16T09:20:00.000Z',
end: '2019-10-16T10:50:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title: ' 12 Technologie internetowe lab',
color:'#1e88e5',
start: '2019-10-16T09:20:00.000Z',
end: '2019-10-16T10:50:00.000Z',
attendees: [ { name: '3S inf B' } ] },
{ title: '13 Podstawy telekomunikacji ćw',
color:'#8e24aa',
start: '2019-10-16T11:10:00.000Z',
end: '2019-10-16T12:40:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title: ' 16 Podstwy systemów operacyjnych lab',
color:'#1e88e5',
start: '2019-10-16T11:10:00.000Z',
end: '2019-10-16T12:40:00.000Z',
attendees: [ { name: '3S inf B' } ] },
{ title: '23/24 Podstwy telekomunikacji w',
color: '#43a047',
start: '2019-10-16T12:50:00.000Z',
end: '2019-10-16T14:20:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '23/24 Technologie internetowe w',
color: '#43a047',
start: '2019-10-16T14:30:00.000Z',
end: '2019-10-16T16:00:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] },
{ title: '13 Języki i metody programowania JAVA lab',
color:'#1e88e5',
start: '2019-10-17T06:00:00.000Z',
end: '2019-10-17T07:30:00.000Z',
attendees: [ { name: '3S inf B' } ] },
{ title: '23 Język angielski ćw',
color:'#8e24aa',
start: '2019-10-17T07:40:00.000Z',
end: '2019-10-17T09:10:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title: '13 Technologie internetowe lab',
color:'#1e88e5',
start: '2019-10-17T07:40:00.000Z',
end: '2019-10-17T09:10:00.000Z',
attendees: [ { name: '3S inf B' } ] },
{ title: '13 Języki i metody programowania JAVA lab',
color:'#1e88e5',
start: '2019-10-17T09:20:00.000Z',
end: '2019-10-17T10:50:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title: '13 Technologie internetowe lab',
color:'#1e88e5',
start: '2019-10-17T11:10:00.000Z',
end: '2019-10-17T12:40:00.000Z',
attendees: [ { name: '3S inf A' } ] },
{ title:
 'hala "B" /Młodych Techników ZS 18/ zajęcia doskonalące dla osób niepełnosprawnych i UKS',
start: '2019-10-17T16:00:00.000Z',
end: '2019-10-17T17:30:00.000Z',
attendees: [ { name: [ '3S inf A' ] }, { name: [ '3S inf B' ] } ] } ]