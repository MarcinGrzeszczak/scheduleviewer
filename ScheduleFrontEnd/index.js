const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)
const path = require('path')
const config = require('./config')
const RedisClient = require('./RedisClient')

let events = []
const port = config.dev.mode ? config.dev.port : config.prod.port
const frontendDir = path.join(process.cwd(),'public')

const redisClient = new RedisClient.Client()
redisClient.subscribe(RedisClient.TYPES.EVENTS)
redisClient.messages(data => {
    events = JSON.parse(data)
    io.emit('eventsUpdate',events)
})

app.use(express.static(frontendDir))

app.on('error', err => console.log(`Server Error. Error: ${err}`))

io.on('connection', socket => {
    console.log(socket)
    socket.emit('eventsUpdate',events)
})

//app.listen(port, () => console.log(`Server started on port ${port}`))
server.listen(port, () => {
    console.log(`Server started on port ${port}`)
})