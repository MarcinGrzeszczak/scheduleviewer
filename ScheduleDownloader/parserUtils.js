function isDate(data){
    return  data.match(/\d{4}-\d{2}-\d*/) !== null
}

function isTextIncludes(text, searchArray){
    const upperText = text.toUpperCase()
    let found = false
    searchArray.forEach(search => {
        if(upperText.includes(search.toUpperCase()))
            found = true
    })
    return found
}

module.exports = {
    isDate,
    isTextIncludes
}