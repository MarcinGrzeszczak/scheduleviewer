const parserUtils = require('../parserUtils')
const assert = require('chai').assert
const fs = require('fs')

let suite = {}
describe('#parserUtils', ()=>{
    before(()=>{
        const websiteMockFile = './websiteMock.html'
        suite.websiteMock = fs.readFileSync(websiteMockFile, )
    })
    describe('#isTextIncludes', ()=> {
        it('should return true if word from array exists in text',()=>{
            const sampleText = 'Studia I stopnia stacjonarne'
            const sampleSearchArray = ['test1','stopnia','test2']
            const result = parserUtils.isTextIncludes(sampleText,sampleSearchArray)
            assert.isTrue(result)

        })

         it('should return false if neither word from array not exists in text',()=>{
            const sampleText = 'Studia I stopnia stacjonarne'
            const sampleSearchArray = ['test1','test3','test2']
            const result = parserUtils.isTextIncludes(sampleText,sampleSearchArray)
            assert.isFalse(result)

        })
    }),

    describe('#isDate', ()=> {
        it('should return true if string is date in fromat yyyy-mm-dd', ()=>{
            const sampleDateStrings = '2019-09-13'
            const result = parserUtils.isDate(sampleDateStrings)
            assert.isTrue(result)
        })

        it('should return false if string is not date in fromat yyyy-mm-dd', ()=>{
            const sampleDateStrings = 'sample test'
            const result = parserUtils.isDate(sampleDateStrings)
            assert.isFalse(result)
        })
    })
})