const assert = require('chai').assert
const expect = require('chai').expect

const htmlParser = require('../htmlParser')

describe('#htmlParser', ()=>{
    describe('#isStudyName', ()=>{
        it('should return array of proper names of studies', () => {
            
            //given
            const sampleData = "" +
            "<p>Studia I stopnia stacjonarne</p> \n"+
            "<p>Studia I stopnia niestacjonarne</p> \n"+
            "<p>Studia II stopnia magisterskie</p> \n"+
            "<p>Kierunek Informatyka – studia inżynierskie i magisterskie z przyszłością</p>"

            const expectedResult = ['Studia I stopnia stacjonarne', 'Studia I stopnia niestacjonarne', 'Studia II stopnia magisterskie']

            //when
            const result = htmlParser.parse(sampleData)
            const resultKeys = Object.keys(result)
            //then
            expect(resultKeys).to.eql(expectedResult)
        })
    })
})