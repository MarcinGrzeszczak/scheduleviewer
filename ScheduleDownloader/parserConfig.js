module.exports = {
    keywords : {
        studies: "Studia",
        schedule: "Plan",
        intramural: "stacjonarne",
        extramural: "niestacjonarne",
        master: "magisterskie",
        degree: "stopnia",
        exams: "sesja"
    }
}