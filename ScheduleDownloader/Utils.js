const fetch = require('node-fetch')
const htmlParser =require('./htmlParser')
const _ = require('lodash')

async function getWebsiteData (address){  
     try{
     const data = await fetch(address)
     const textData = data.text()
     return await textData
     }
     catch(err){
        throw new Error(`Error with fetching : \n ${err}`)
     }
}


async function isNewSchedule(websiteAddress, currentSchedulesData) {
    const htmlBody = await getWebsiteData(websiteAddress) 
    const parsedData = htmlParser.parse(htmlBody)

    const difference = _.pickBy(parsedData, (value,key) => !_.isEqual(value,currentSchedulesData[key]))
    return difference 
}

module.exports = {
    isNewSchedule
}