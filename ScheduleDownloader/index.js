const Utils = require('./Utils')
const config = require('./config.json')
const RedisClient = require('./RedisClient'
)
const completeURL = config['website'] + config['subSite']

const client = new RedisClient.Client()

const isNewSchedule = Utils.isNewSchedule.bind(this, completeURL)

let sampleData = { 'Studia I stopnia stacjonarne':
{ link:
   'https://www.wsiz.wroc.pl/wp-content/uploads/2019/10/plan_stacjonarny-9.xls',
  date: '2019-10-17' },
'Studia I stopnia niestacjonarne':
{ link:
   'https://www.wsiz.wroc.pl/wp-content/uploads/2019/10/plan-niestacjonarny-14.xls',
  date: '2019-09-17' },
'Studia II stopnia magisterskie':
{ link:
   'https://www.wsiz.wroc.pl/wp-content/uploads/2019/10/plan-niestacjonarny-14.xls',
  date: '2019-09-17' } }

isNewSchedule(sampleData)
  .then(data => { 
    console.log(data)
    client.set(RedisClient.KEYS.STUDIES_TYPE, Object.keys(data))
  })

  