const redis = require('redis')
const { promisify } = require("util");

class Client {
    constructor() {
        this.client = redis.createClient({
            host: 'redis-server'
        })

        this.client.on("error", function(err) {
            console.error(err)
        })
    }

    set(key,data) {
        this.client.set(key,JSON.stringify(data),redis.print)
    }

    async get(key) {
        const asyncGet = promisify(this.client.get).bind(this.client)
        return await asyncGet(key)
    }
}

const KEYS = {
    STUDIES_TYPE: 'studiestype'
}

module.exports = {
    Client,
    KEYS
}