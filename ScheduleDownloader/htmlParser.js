const htmlparser2 = require('htmlparser2')
const config = require('./parserConfig')
const utils = require('./parserUtils')
let isText = false
let isSchedule = false
let parsedData = {}
let currentName = null
let isColumn = false


const parser = new htmlparser2.Parser({ 
    onopentag(name, attribs){

        switch(name){
            case 'a': addLinkToObject(attribs); break
            case 'p': isText = true; break
            case 'td': isColumn = true; break
        }
    },

    ontext(text){
        if(isText && isStudyName(text)){
            currentName = text
            parsedData[currentName] =  {
                link: '',
                name: '',
                date: ''
            }
            isText = false
            isSchedule = false
        }
        
        if(isColumn && currentName && isSchedule && utils.isDate(text)){
            parsedData[currentName].date = text
            isColumn = false
            isSchedule = false

        } else if(isColumn && utils.isTextIncludes(text,[config.keywords.schedule]))
            isSchedule = true
    }

})

function isStudyName(text){
    return utils.isTextIncludes(text, [config.keywords.studies]) && 
     utils.isTextIncludes(text, [config.keywords.intramural,config.keywords.extramural,config.keywords.master]) &&
     utils.isTextIncludes(text, [config.keywords.degree])
 
 }

function addLinkToObject(attribs){
    if(!currentName || !parsedData[currentName])
        return

    const link = attribs.href
    if(utils.isTextIncludes(link,[config.keywords.schedule])) {
        parsedData[currentName].link = link
        addFileNameToObject(link)
    }
}

function addFileNameToObject(link) {
    const parts = link.split('/')
    parsedData[currentName].name = parts[parts.length - 1]
}

function parse(data){
    parser.write(data)
    parser.end()
    return parsedData
}

module.exports = {
    parse
}