const _ = require('lodash')



class ParserUtils {
    
    static getSheetNamesWithDates(sheetNames){
        return _.filter(sheetNames, name => name.match(/([1-3]?[0-9])(-|.*-)([1-3]?[0-9])(\.).*/))
    }

    static getCellValue(sheet, cell){
        if (!_.has(sheet[cell],'v'))
            return null
        
        const value = sheet[cell].v
        return value
    }
    
    static separateColumnFromRow(data) {
        const split = data.match(/(\D+)(\d+)\s*/)
        return {
            column: split[1],
            row: split[2]
        }
    }

    static sortSheet(sheet) {
        let ordered = {}
        Object.keys(sheet)
            .sort( (a, b) => a.localeCompare(b,undefined,{numeric:true}))
            .forEach(key => ordered[key] = sheet[key])
        return ordered
    }

    static addDataToCell(sheet, cell, value) {
        sheet[cell] = {
            v: value,
            t: 's',
            w: value
        }
        return sheet
    }

    static getColumnRange(startCell, endCell) {
        const startColumn = this.separateColumnFromRow(startCell).column
        const endColumn = this.separateColumnFromRow(endCell).column

        const numRange = _.range(this.getNumberFromLetter(startColumn), this.getNumberFromLetter(endColumn)+1)
        return numRange.map(num => this.getLetterFromNumber(num,true))
    }

    static fillMissingCells(range) {
        const startSplit = this.separateColumnFromRow(range[0])
        const columnLetter = startSplit.column
        const startNumber = startSplit.row

        const endSplit = this.separateColumnFromRow(range[range.length-1])
        const endNumber = endSplit.row

        return _.range(startNumber,endNumber).map(val => columnLetter+val)
    }

    static filterKey(sheet ,columns, rows){
        const wildcard = '*'
        let columnExp = columns
        let rowExp = rows+'$'

        if (columns === wildcard)
            columnExp = '[A-Z]'
        
        if (rows === wildcard)
            rowExp = ''

        const regExpFilter = `^${columnExp}${rowExp}`

        return  _.filter(_.keys(sheet), key => key.match(regExpFilter))
    }

    static getMerge(merges, startCell) {
        const startColumn = this.getNumberFromLetter(startCell[0])
        const startRow =  startCell.substring(1, startCell.length) - 1

        const query = {
            s: {
                c: startColumn,
                r: startRow
            }
        }

        const mergeData = _.find(merges, query)      
        return mergeData
    }

    static getMergedLastCell(merges, startCell){
        const mergeData = this.getMerge(merges, startCell)
        if (mergeData === undefined)
        return null
    
        const columnNumber = mergeData.e.c
        const rowNumber = mergeData.e.r + 1 
        const lastCell = `${this.getLetterFromNumber(columnNumber,true)}${rowNumber}`
        return lastCell
    }

    static getNumberFromLetter(letter){
        const asciiStartNumber = letter === letter.toUpperCase()? 65 : 97      
        return letter.charCodeAt(0) - asciiStartNumber
    }

    static getLetterFromNumber(number, isCapital){
        const asciiStartNumber = isCapital ? 65 : 97
        return String.fromCharCode(number + asciiStartNumber)
    }

    static trimSheetByColumns(sheet, startColumn, endColumn){
       const keys = this.filterKey(sheet, `[${startColumn}-${endColumn}]`, '*')
       
       return _.pick(sheet, keys)
    }

    static isDate(value) {

        return value.match(/^(PONIEDZIAŁEK|WTOREK|ŚRODA|CZWARTEK|PIĄTEK)\s+(\d+)\.(\d+)(\.\d+)?/) !== null
    }

    static parseDate(dateString) {
        const date = dateString.match(/^(PONIEDZIAŁEK|WTOREK|ŚRODA|CZWARTEK|PIĄTEK)\s+(\d+)\.(\d+)(\.\d+)?/)
        const day = date[2]
        const month = date[3] - 1
        const year = date[4] ? date[4].substring(date[1], date[4].length) : new Date().getFullYear()
        return {
            day,
            month,
            year
        }
    }

    static isTime(value){
        return value.match(/^([0-9]?[0-9]).([0-6]?[0-9])\s*do\s*([0-9]?[0-9]).([0-6]?[0-9])/) !== null

    }

    static parseTime(timeString){
        const time = timeString.match(/^([0-9]?[0-9]).([0-6]?[0-9])\s*do\s*([0-9]?[0-9]).([0-6]?[0-9])/)

        return {
            start: {
                hour: time[1],
                minutes: time[2]
            },
            end: {
                hour: time[3],
                minutes: time[4]
            }
        }
    
    }

    static findCells(data, search) {
        const cells = _.keys (
            _.pickBy(data, value => _.includes(value.v, search))
        )

        return cells
    }

    static populateLetterArray(startLetter, endLetter){
        const asciiStartNumber = startLetter.charCodeAt(0)
        const asciiEndNumber = endLetter.charCodeAt(0)
        let letterArray = []
        for(let currentNumber = asciiStartNumber ; currentNumber <= asciiEndNumber ; ++currentNumber)
            letterArray.push(String.fromCharCode(currentNumber))
        
        return letterArray
    }

    static duplicateDataInRange(range, data, value) {
        
    }

}

module.exports = ParserUtils