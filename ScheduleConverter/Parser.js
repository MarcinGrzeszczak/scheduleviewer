const ParserUtils = require('./ParserUtils')
const parserConfig  = require('./parserConfig')
const moment = require('moment')
const _ = require('lodash')

function getSemesters(sheet, sheetMerges) {
    const semesterCells = ParserUtils.findCells(sheet, parserConfig.keywords.semester)

    let parsedSemesters = {}

    semesterCells.forEach(cell => {
        const semesterName = ParserUtils.getCellValue(sheet, cell)
        const lastCell = ParserUtils.getMergedLastCell(sheetMerges, cell)
        Object.assign(parsedSemesters, {
          [semesterName] : {
            firstCell : cell,
            lastCell
        }
    })
})

    return parsedSemesters
}

function trimSemester(sheet ,semester){
    const firstColumn = semester.firstCell.charAt(0)
    const lastColumn = semester.lastCell.charAt(0)
    let trimmedData = ParserUtils.trimSheetByColumns(sheet, firstColumn, lastColumn)
    const column = ParserUtils.filterKey(trimmedData, firstColumn, '*')
    const allCellsInColumn = ParserUtils.fillMissingCells(column)
    const columnRange = ParserUtils.getColumnRange(semester.firstCell, semester.lastCell)
    
    allCellsInColumn.forEach(cell => {
        const value = ParserUtils.getCellValue(trimmedData,cell)
        if(value === null) {
            
            const row = ParserUtils.separateColumnFromRow(cell).row
            const firstCellWithVal = columnRange
                .map(col => `${col}${row}`)
                .filter(c => ParserUtils.getCellValue(trimmedData,c) !== null)
            
            const firstVal = firstCellWithVal.length >0 ? ParserUtils.getCellValue(trimmedData,firstCellWithVal[0]): null
        
            if(firstVal !== null){
                trimmedData = ParserUtils.addDataToCell(trimmedData,cell,firstVal)
            }
        }
    })
    return ParserUtils.sortSheet(trimmedData)
}

function parseGroups(trimmedData){
    let parsedGroups = {}
    const groupCells =  ParserUtils.findCells(trimmedData, parserConfig.keywords.group)
    if (groupCells.length === 0)
        return null

    
    const groupKeywordCell = groupCells[0]
    
    const rowWithGroupNames = groupKeywordCell.substring(1,groupKeywordCell.length)

    const groupRow = ParserUtils.filterKey(trimmedData,'*',rowWithGroupNames)

    groupRow.shift()

    groupRow.forEach( groupCell => {
        const groupName = ParserUtils.getCellValue(trimmedData, groupCell)
        Object.assign(parsedGroups, {
            [groupName] : {
                column : groupCell.charAt(0)
            }
        })
    })

    return parsedGroups
}

function parseDatesAndTimeArray(trimmedData, semester) {
    let parsedTimeDataArray = []
    const columnLetter = semester.firstCell.charAt(0)
    const allCellsInColumn = ParserUtils.filterKey(trimmedData, columnLetter, '*')
    let parsedDate = {}
    allCellsInColumn.forEach( cell => {
        const value = ParserUtils.getCellValue(trimmedData, cell)
        if(ParserUtils.isDate(value)){
            parsedDate = ParserUtils.parseDate(value) 
            return
        }

        if(ParserUtils.isTime(value)){
           const parsedTime =  ParserUtils.parseTime(value)
           parsedTimeDataArray.push({
                startDate: moment(_.merge(parsedDate, parsedTime.start)).toISOString(),
                endDate: moment(_.merge(parsedDate, parsedTime.end)).toISOString(),
                cell
           })
        }

    })
    return parsedTimeDataArray
}

//TODO simplify this method and make more genere
function parseEvents(trimmedData, merges ,parsedTimeDataArray, semesterGroups){
    let parsedEventsArray = []
    parsedTimeDataArray.forEach( timeData => {
        const selectedRow = timeData.cell.substring(1,timeData.cell.length)
        
        Object.keys(semesterGroups).forEach(groupName => {
            const group = semesterGroups[groupName]
            const currentCell = `${group.column}${selectedRow}`
            const cellValue = ParserUtils.getCellValue(trimmedData, currentCell)
            
            if(!cellValue || cellValue === parserConfig.keywords.break)
                return
            
            let attendees = []
            
            const lastMergedCell = ParserUtils.getMergedLastCell(merges, currentCell)
            if( lastMergedCell !== null){
                const attendeesColumnArray =  ParserUtils.populateLetterArray(currentCell, lastMergedCell)
                attendeesColumnArray.forEach(attendeeColumn =>{
                    attendees.push({
                        name:  _.keys(_.pickBy(semesterGroups, value => _.includes(value.column, attendeeColumn)))
                    })
                   
                })
                
            }
            else
                attendees.push({
                    name: groupName
                })
              
            parsedEventsArray.push({
               title: cellValue,
               start: timeData.startDate,                                                                                                                                                                                                                     
               end: timeData.endDate,
               attendees
            })
        })
    })
    return parsedEventsArray
}

module.exports = {
    getSemesters,
    trimSemester,
    parseGroups,
    parseEvents,
    parseDatesAndTimeArray
}