const XLSX = require('xlsx')

const ERRORS = {
    EMPTY_SHEET : Symbol('Please load sheet first'),
    EMPTY_WORKBOOK : Symbol('Please load workbook first')
}

class XlsxLoader {
    constructor(){
        this.workbook = null
        this.sheet = null
    }

    readWorkbook(filepath) {
        this.workbook = XLSX.readFile(filepath)
        return this.workbook
    }

    readSheet(sheetName) {
        if(!this.workbook)
            throw new Error(ERRORS.EMPTY_WORKBOOK)
        this.sheet = this.workbook.Sheets[sheetName]
        return this.sheet
    }

    getSheetNames() {
        if(!this.workbook)
            throw new Error(ERRORS.EMPTY_WORKBOOK)
        
        return this.workbook.SheetNames
    }

    getSheetMerges() {
        if(!this.sheet)
            throw new Error(ERRORS.EMPTY_SHEET)
        
        return this.sheet["!merges"]
    }
}

module.exports = XlsxLoader