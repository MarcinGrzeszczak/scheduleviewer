const http = require('http')
const XlsxLoader = require('./XlsxLoader')
const Parser = require('./Parser')
const ParserUtils = require('./ParserUtils')
const RedisClient = require('./RedisClient')

const xlsxLoader = new XlsxLoader()

const publisherClient = new RedisClient.Client()


function getSheetsWithDates() {
    const allSheetNames = xlsxLoader.getSheetNames()
    return ParserUtils.getSheetNamesWithDates(allSheetNames)
}

function getSheetEvents(sheet) {
    let currentSheet = xlsxLoader.readSheet(sheet)
    let currentMerges = xlsxLoader.getSheetMerges()
    
    const semesters = Parser.getSemesters(currentSheet, currentMerges)
    
    const trimmedData = Parser.trimSemester(currentSheet, semesters['SEMESTR 4'])
    
    const groups = Parser.parseGroups(trimmedData)
    
    const timeDataArray = Parser.parseDatesAndTimeArray(trimmedData, semesters['SEMESTR 4'])
    
    const eventArray = Parser.parseEvents(trimmedData,currentMerges,timeDataArray, groups)

    return eventArray
}
function parseEventArray() {
    xlsxLoader.readWorkbook('data/plan_stacjonarny.xls')
    const sheetsWithDate = getSheetsWithDates()
    const allEvents = []

    sheetsWithDate.forEach(sheetName => {
        allEvents.push(...getSheetEvents(sheetName))
    })
    return allEvents
}

const eventArray = parseEventArray()
console.log(eventArray)
publisherClient.publish(RedisClient.TYPES.EVENTS, JSON.stringify(eventArray))

http.createServer().listen('10000', () => console.log('converter started'))