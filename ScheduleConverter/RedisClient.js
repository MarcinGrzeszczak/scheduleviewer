const redis = require('redis')
class Client {
    constructor() {
        this.client = redis.createClient({
            host: 'redis-server'
        })
        this.client.on("error", function(error) {
            console.error(error);
          });
          
    }

    publish(channel, data){
        this.client.publish(channel,data)
    }

    quit() {
        this.client.quit()
    }
}
const TYPES = {
    EVENTS: 'events'
}
module.exports = {
    Client,
    TYPES
}